# LiveClone

Simplifies the creation of a Live CD or a Live USB on SalixLive or Salix (Unofficial repo)

![](http://zenway.ru/uploads/salix/liveclone-256.png)

LiveClone simplifies the creation of a Live CD or a Live USB
key based on SalixLive or Salix standard running environment.
All this from the comfort of a graphical interface.

http://people.salixos.org/djemos/salix/liveclone-salix/

Утилита LiveClone позволяет легко и просто создать свой собственный LiveCD и/или LiveUSB из настроенного
по собственным потребностям Salix Live (можно добавить/удалить приложения, темы, настройки и прочее). 

![](http://zenway.ru/uploads/salix/live/salixlive-mate_023.jpeg)
